# Pizza ordering REST API

## Summary: 
Create a REST API for pizza orders with an open API spec and swagger documentation using NestJS.

The API should cover the following endpoints:
Users endpoint
Restaurants endpoint
Orders endpoint
Pizzas endpoint

Implement support for the following HTTP 1.1. API methods with matching status codes:
GET
POST
PUT
PATCH
DELETE

Also, the following user roles exist:
user
restaurant

## NOTE: 
Not all endpoints need to support all API methods. Implement what you think is required.

# Endpoints
## Users endpoint
Users should be able to register for a new account in the system. Successful registration should return a valid authentication token like you would get when you log in.

It should also allow users to log in to retrieve and refresh their tokens. Authentication should follow OAuth 2.0 standard and support both short and long-lived (refresh) tokens.

When registering, the user should be able to select the account type (restaurant or user). Users cannot be both account types at the same time.

Also allows users to check roles. There are 2 roles for authenticated users (user and restaurant).

## Restaurants endpoint
The list of restaurants should be public to all users regardless of their type.

Only a restaurant user can manage or delete their own data.

Seed 2-3 restaurants in the database upon first application startup or any other means such as seed scripts (preferred)

Restaurants are only making pizzas and there are no other meals on the menu.

## Orders endpoints
Allows creation and management of pizza orders in the system.

The creation of a new order can be done by an anonymous user and a registered one.

## Anonymous users
Anonymous users can create a new order and should be able to cancel or modify it as long as they provide a valid modification token that allows them to authenticate that change.

This token should be generated upon order by an anonymous user and expire after the order has been accepted or declined by the restaurant to disallow users any changes to the order after its expiry.

## Registered users
Registered users can create new orders. They should be able to cancel or modify their orders until the restaurant accepts or declines them.

Registered users authenticate their changes with their valid login token. No additional change needs to be generated for this type of user.

Registered users should be able to retrieve their order history.

Registered users will have their address tied to their account upon first successful order or after updating their user account with their info.

## NOTE: 
The list of orders is not public. It is only visible in the following scenarios:
User can see their own orders
Restaurant users can see orders placed to their restaurant



## Pizzas endpoint
A restaurant pizza menu should be publicly available.

A pizza menu consists of various pizza types, and they consist of ingredients.

Only restaurant users can manage or delete their own pizzas and their ingredients. 

You can seed pizzas and their ingredients for test restaurants in the database upon first application startup or any other means such as seed scripts (preferred)


# Other project specifics
## Framework
Please implement the solution using the nest.js framework.

The choice of the framework comes down to its impressive core feature set, usage at PROTOTYP and great TypeScript support.

Usage of TypeScript in the project is mandatory.

## Filtering
API should support filtering for product details through route parameters but all other filters such as paging, perPage etc through query parameters. Query parameters in these instances should have their defaults set, which is up to you to decide on.


## DB/ORM
Use PostgreSQL database with Sequelize ORM for the project

Utilize repository pattern for SequelizeORM which is further described inside Nest.js documentation.

Primary ID-s for tables should be of UUID type, not integers or strings due to PostgreSQL native support for this data type.

All database entities should support tracking of creation time, update times and soft-deletions.

The database should be created through migrations and data seeding is something that we would like to see how you will accomplish.

## Tests
All endpoint operations should implement at least basic tests that nest.js CLI creates boilerplate for. 

## Architectural suggestions
Feel free to show us what your ideal project looks like. We always strive for greater modularity in our approach to code.

However, please do not go too far from the basic nest.js patterns, as this kinda makes their CLI useless, and it is a great productivity tool.


## Versioning
API should support versioning. We would like you to explain in which cases the lack of versioning can be a deal breaker for the product.


## Linting/Formatting
Nest.js has good out of the box support for eslint and prettier. Please use these, as sloppy and inconsistent code leads to sloppy solutions. We would like to work with candidates who know how to be consistent inside a team environment.

## Exception handling
We do prefer having a global exception handler on our API-s in order to consistently provide adequate HTTP response codes for the same errors across the board.

Check out mechanisms for exception handling in NestJS and try to find an optimal one to support this functionality.

We would also like to see how you manage exception throwing inside your asynchronous code.
